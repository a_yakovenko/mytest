<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 17:30
 */

namespace models\repository;

use models\entities\employee\Employee;
use models\entities\employee\EmployeeId;

interface Repository
{
    /**
     * @param EmployeeId $id
     * @return Employee
     */
    public function get(EmployeeId $id);

    /**
     * @return array
     */
    public function getAll();

    /**
     * @param Employee $employee
     */
    public function add(Employee $employee);

    /**
     * @param Employee $employee
     */
    public function save(Employee $employee);

    /**
     * @param Employee $employee
     */
    public function remove(Employee $employee);
}