<?php
namespace models\repository;

use models\entities\employee\EmployeeDepartment;
use models\entities\employee\EmployeeId;

class DepartmentsRepository
{
    private static $mysqli;

    public function __construct($id = null)
    {
        self::$mysqli = DataBase::getInstance();
    }


    public function getAll() {
        if(self::$mysqli->ping()) {

            $result = self::$mysqli->query("SELECT * FROM `department`");
            if ($result === false) {
                throw new \Exception(self::$mysqli->error);
            }

            $departments = [];
            foreach ($result->fetch_all(MYSQLI_ASSOC) as $item) {
                $departments[$item['id']] = $item['title'];
            }

            return $departments;
        }
        return false;
    }

    public function update($id, $title) {
        try {
            if(self::$mysqli->ping()) {

                $mysqliStmt	= self::$mysqli->stmt_init();

                $mysqliStmt->prepare(
                    "UPDATE `department` SET
                    `title` = ?
                    WHERE `id` = ?"
                );
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }

                $mysqliStmt->bind_param('si',
                    $title,
                    $id
                );
                $mysqliStmt->execute();
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }
                return true;
            }
        } catch (\Exception $e) {
            $_SESSION["errorFlashMessage"] = $e->getMessage();
        }
        return false;
    }

    public function get($id) {
        if(self::$mysqli->ping()) {

            $mysqliStmt	= self::$mysqli->stmt_init();
            $mysqliStmt->prepare("
            SELECT * FROM `department`
            WHERE `id` = ?
            ");

            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            $mysqliStmt->bind_param('i', $id);
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $result = $mysqliStmt->get_result();
            if (!$result->num_rows) {
                throw new \DomainException('Department not Found');
            }

            return $result->fetch_assoc();
        }
        return false;
    }

    public function add($title)
    {
        try {
            if(self::$mysqli->ping()) {
                $mysqliStmt	= self::$mysqli->stmt_init();

                $mysqliStmt->prepare(
                    "INSERT INTO `department` (`id`, `title`)
                      VALUES (NULL, ?)"
                );
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }
                $mysqliStmt->bind_param('s', $title);
                $mysqliStmt->execute();
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }
                return true;
            }
        } catch (\Exception $e) {
            $_SESSION["errorFlashMessage"] = $e->getMessage();
        }
        return false;
    }
    
    
    public function remove($id)
    {
        try {
            if(self::$mysqli->ping()) {

                $mysqliStmt	= self::$mysqli->stmt_init();
                $mysqliStmt->prepare(
                    "DELETE FROM `department` 
				WHERE `id` = ?"
                );
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }
                $mysqliStmt->bind_param('i', $id);
                $mysqliStmt->execute();
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }
                return true;
            }
        } catch (\Exception $e) {
            if (preg_match('/foreign key constraint fails/', $e->getMessage())) {
                $_SESSION["errorFlashMessage"] = 'This department is assigned to employees and cannot be deleted.';
            } else {
                $_SESSION["errorFlashMessage"] = $e->getMessage();
            }
        }
        return false;
    }
}