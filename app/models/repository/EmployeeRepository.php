<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 17:33
 */

namespace models\repository;

use models\entities\employee\{
    Employee, EmployeeDate, EmployeeDateForFired, EmployeeDepartment, EmployeeEmail, EmployeeId, EmployeeLogin, EmployeeName, EmployeePassword, EmployeePhone, EmployeeRole, EmployeeStatus
};

class EmployeeRepository implements Repository
{
    private static $mysqli;

    public function __construct($id = null)
    {
        self::$mysqli = DataBase::getInstance();
    }

    /**
     * @param Employee $employee
     * @return bool
     * @throws \Exception
     */
    private function updateEmployeeDepartments(Employee $employee)
    {
        if(self::$mysqli->ping()) {

            $mysqliStmt	= self::$mysqli->stmt_init();
            $mysqliStmt->prepare("DELETE FROM `users_department` WHERE `user_id` = ?");
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            $id = $employee->getId()->getId();
            $mysqliStmt->bind_param('i', $id);
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }


            $mysqliStmt	= self::$mysqli->stmt_init();
            $mysqliStmt->prepare("
	    		INSERT INTO `users_department` (id, user_id, department_id) 
				VALUES (NULL, ?,?)"
            );
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            $id = $employee->getId()->getId();
            foreach ($employee->getDepartments() as $item) {
                $id_department = $item->getDepartmentID();
                $mysqliStmt->bind_param('ii', $id, $id_department);
                $mysqliStmt->execute();
                if ($mysqliStmt->errno) {
                    throw new \Exception(self::$mysqli->error);
                }
            }
            
            return true;
        }
        return false;
    }

    public function add(Employee $employee)
    {
        try {
            // check - if user with the same login exist
            if ($this->getIdByLogin($_POST['login'])) {
                throw new \DomainException('User with the same login already exist.');
            }

            self::$mysqli->autocommit(FALSE);
            $mysqliStmt	= self::$mysqli->stmt_init();

            $mysqliStmt->prepare(
                "INSERT INTO `users` (`id`, `login`, `password`, `last`, `first`, `middle`, `date_birth`, `email`, `phone`, `date_employment`, `date_fired`, `status`, `roles`)
                  VALUES (NULL, ?,?,?,?,?,?,?,?,?,?,?,?)"
            );
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $login = $employee->getLogin()->getLogin();
            $password = $employee->getPassword()->getPassword();
            $last = $employee->getName()->getLast();
            $first = $employee->getName()->getFirst();
            $middle = $employee->getName()->getMiddle();
            $date_birth = $employee->getDateBirth()->getDate();
            $email = $employee->getEmail()->getEmail();
            $phone = $employee->getPhone()->getPhone();
            $date_employment = $employee->getDateEmployment()->getDate();
            $date_fired = $employee->getDateFired()->getDate();
            $status = $employee->getStatus()->getStatus();
            $role = $employee->getRole()->getRole();

            $mysqliStmt->bind_param('ssssssssssii',
                $login,
                $password,
                $last,
                $first,
                $middle,
                $date_birth,
                $email,
                $phone,
                $date_employment,
                $date_fired,
                $status,
                $role
            );
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $employee->getId()->setId($mysqliStmt->insert_id);
            $this->updateEmployeeDepartments($employee);
            self::$mysqli->commit();
            return true;

        } catch(\Exception $e) {
            self::$mysqli->rollback();
            $_SESSION["errorSingleFlashMessage"] = $e->getMessage();
        }
    }


    public function save(Employee $employee)
    {
        try {
            self::$mysqli->autocommit(FALSE);
            $mysqliStmt	= self::$mysqli->stmt_init();

            $mysqliStmt->prepare(
                "UPDATE `users` SET
			   		`last` = ?,
			    	`first` = ?,
			    	`middle` = ?,
			    	`date_birth` = ?,
			    	`email` = ?,
			    	`phone` = ?,
			    	`date_employment` = ?,
			    	`date_fired` = ?,
			    	`status` = ?,
			    	`roles` = ?
					WHERE `id` = ?"
            );
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $last = $employee->getName()->getLast();
            $first = $employee->getName()->getFirst();
            $middle = $employee->getName()->getMiddle();
            $date_birth = $employee->getDateBirth()->getDate();
            $email = $employee->getEmail()->getEmail();
            $phone = $employee->getPhone()->getPhone();
            $date_employment = $employee->getDateEmployment()->getDate();
            $date_fired = $employee->getDateFired()->getDate();
            $status = $employee->getStatus()->getStatus();
            $role = $employee->getRole()->getRole();
            $id = $employee->getId()->getId();

            $mysqliStmt->bind_param('ssssssssiii',
                $last,
                $first,
                $middle,
                $date_birth,
                $email,
                $phone,
                $date_employment,
                $date_fired,
                $status,
                $role,
                $id
            );
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $this->updateEmployeeDepartments($employee);
            self::$mysqli->commit();
            return true;

        } catch(\Exception $e) {
            self::$mysqli->rollback();
            $_SESSION["errorSingleFlashMessage"] = $e->getMessage();
        }
    }
    public function remove(Employee $employee)
    {
        if(self::$mysqli->ping()) {

            $mysqliStmt	= self::$mysqli->stmt_init();
            $mysqliStmt->prepare(
                "DELETE FROM `users` 
				WHERE `id` = ?"
            );
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            $id = $employee->getId()->getId();
            $mysqliStmt->bind_param('i', $id);
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            return true;
        }
        return false;
    }


    public function get(EmployeeId $id) {
        if(self::$mysqli->ping()) {

            $mysqliStmt	= self::$mysqli->stmt_init();
            $mysqliStmt->prepare("SELECT users.*,
            GROUP_CONCAT(department.id ORDER BY department.id ASC SEPARATOR ',') AS departmentIds
            FROM users
            INNER JOIN users_department ON users.id = users_department.user_id
            INNER JOIN department ON department.id = users_department.department_id
            WHERE users.id = ?
            GROUP BY (users.id)
            ");

            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            $id = $id->getId();
            $mysqliStmt->bind_param('i', $id);
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $result = $mysqliStmt->get_result();
            if (!$result->num_rows) {
                throw new \DomainException('User not Found');
            }

            return $result->fetch_assoc();
        }
        return false;
    }


    public function getAll()
    {
        if(self::$mysqli->ping()) {

            $sqlWhereString = '';
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if ($_POST['name']) {
                    $names = explode(' ', trim($_POST['name']));
                    foreach ($names as $name) {
                        $sqlWhereString .= " AND (last LIKE '%$name%' OR first LIKE '%$name%' OR middle LIKE '%$name%')";
                    }
                }
                if ($_POST['email']) {
                    $email = trim($_POST['email']);
                    $sqlWhereString .= " AND email LIKE '%$email%'";
                }

                if ($_POST['phone']) {
                    $phone = trim($_POST['phone']);
                    $sqlWhereString .= " AND phone LIKE '%$phone%'";
                }

                if ($_POST['status']) {
                    $status = trim($_POST['status']);
                    $sqlWhereString .= " AND status = $status";
                }

                if ($_POST['department']) {
                    $department = trim($_POST['department']);
                    $sqlWhereString .=
                        " INNER JOIN users_department AS users_department2 ON users.id = users_department2.user_id
                        INNER JOIN department AS department2 ON department2.id = users_department2.department_id
                        AND department2.id = $department ";
                }
            }

            $result = self::$mysqli->query("
            SELECT users.*,
            GROUP_CONCAT(department1.id ORDER BY department1.id ASC SEPARATOR ',') AS departmentIds
            FROM users
            INNER JOIN users_department AS users_department1 ON users.id = users_department1.user_id
            INNER JOIN department AS department1 ON department1.id = users_department1.department_id"
            . $sqlWhereString .
            " GROUP BY (users.id)
            ");
            if ($result === false) {
                throw new \Exception(self::$mysqli->error);
            }
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }

    public function getIdByLogin($login) {
        if(self::$mysqli->ping()) {

            $mysqliStmt	= self::$mysqli->stmt_init();
            $mysqliStmt->prepare("SELECT * FROM `users` WHERE `login` = ?");

            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }
            $mysqliStmt->bind_param('s', $login);
            $mysqliStmt->execute();
            if ($mysqliStmt->errno) {
                throw new \Exception(self::$mysqli->error);
            }

            $result = $mysqliStmt->get_result()->fetch_assoc();

            return $result['id'];
        }
        return false;
    }
}