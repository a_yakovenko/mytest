<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 17:36
 */

namespace models\repository;

class DataBase
{
    private static $dbHost = SERVER_NAME;
    private static $dbUser = USER_NAME;
    private static $dbpass = PASSWORD;
    private static $dbname = DB_NAME;

    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new \mysqli(self::$dbHost, self::$dbUser, self::$dbpass, self::$dbname);
            self::$instance->set_charset('utf8');

            if (self::$instance->connect_errno) {
                die("Failed to connect to Database: (" . self::$instance->connect_errno . ") " . self::$instance->connect_error);
            }
        }
        return self::$instance;
    }

    private function __construct()	{}
    private function __clone() 		{}
    private function __wakeup() 	{}
}