<?php

namespace models;


class Helper
{
    public static function deleteFlashes()
    {
        unset($_SESSION["errorFlashMessage"]);
        unset($_SESSION["errorSingleFlashMessage"]);
        unset($_SESSION["flashMessage"]);
    }

    public static function goToMainPage()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('Location:' . $host);
        exit();
    }
}