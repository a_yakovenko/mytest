<?php

namespace models;

use models\entities\employee\EmployeeBuilder;
use models\entities\employee\EmployeeId;
use models\repository\EmployeeRepository;

class User
{
    public $employee;

    public function __construct($userID)
    {
        $employeeRepository = new EmployeeRepository();
        $employeeBuilder = new EmployeeBuilder();
        
        $employee = $employeeRepository->get(new EmployeeId($userID));
        $user = $employeeBuilder->build($employee);
        $user->getPassword()->setPassword($employee['password']);

        $this->employee = $user;
    }

    public function rememberUser() {
        $_SESSION['login'] = $this->employee->getLogin()->getLogin();
        $_SESSION['isAdmin'] = $this->employee->isAdmin();
    }

    public static function forgotUser() {
        session_unset();
    }

    public static function getLoginName() {
        return $_SESSION['login'] ?? false;
    }

    public static function isLoggedIn() {
        return $_SESSION['login'] ?? false;
    }

    public static function isAdmin() {
        return $_SESSION['isAdmin'] ?? false;
    }
}