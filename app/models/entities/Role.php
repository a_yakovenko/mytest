<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 13:54
 */

namespace models\entities;


abstract class Role
{
    const USER = 1;
    const ADMIN = 2;

    private $role;

    public function __construct(int $role = self::USER)
    {
        if (!in_array(
            $role,
            [
                self::USER,
                self::ADMIN
            ]
        )) {
            throw new \InvalidArgumentException('Value "role" is not included in the available array.');
        }

        $this->role = $role;
    }

    public function isAdmin(): bool
    {
        if ($this->role == self::ADMIN) return true;

        return false;
    }

    public function getRole()
    {
        return $this->role;
    }

    public static function getAllRoles()
    {
        return [
            self::USER => 'User',
            self::ADMIN => 'Admin'
        ];
    }
}