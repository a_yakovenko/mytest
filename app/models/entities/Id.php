<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 19:59
 */

namespace models\entities;


abstract class Id
{
    protected $id;

    public function __construct($id = null)
    {
        $this->id = $id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function isEqualTo(self $other): bool
    {
        return $this->getId() === $other->getId();
    }
}