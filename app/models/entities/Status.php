<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 20:06
 */

namespace models\entities;


abstract class Status
{
    const NOT_ACTIVE = 2;
    const ACTIVE = 1;

    protected $status;

    public function __construct(int $status = self::ACTIVE)
    {
        if (!in_array(
            $status,
            [
                self::NOT_ACTIVE,
                self::ACTIVE
            ]
        )) {
            throw new \InvalidArgumentException('Value "status" is not included in the available array.');
        }

        $this->status = $status;
    }

    public function isActive(): bool
    {
        if ($this->status == self::ACTIVE) return true;

        return false;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public static function getAllStatuses()
    {
        return [
            self::ACTIVE => 'Активный сотрудник',
            self::NOT_ACTIVE => 'Не активный сотруднивк'
        ];
    }
}