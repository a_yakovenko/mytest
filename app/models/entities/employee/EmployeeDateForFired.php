<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 21:27
 */

namespace models\entities\employee;


class EmployeeDateForFired extends EmployeeDate
{
    public function __construct($date = null)
    {
        if ($date != null) {
            if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {
                throw new \InvalidArgumentException('Value "date" has wrong format.');
            }
            $this->date = $date;
        } else {
            $this->date = null;
        }
    }
}