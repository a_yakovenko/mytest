<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 20:42
 */

namespace models\entities\employee;


class EmployeeDate
{
    protected $date;

    public function __construct($date = null)
    {
        if (empty($date)) {
            throw new \InvalidArgumentException('Value "date" can not be empty.');
        }

        if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {
            throw new \InvalidArgumentException('Value "date" has wrong format.');
        }

        $this->date = $date;
    }

    public function getDate() { return $this->date; }
}