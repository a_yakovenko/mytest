<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 21:14
 */

namespace models\entities\employee;


class EmployeePhone
{
    private $phone;

    public function __construct($phone = null)
    {
        if (empty($phone)) {
            throw new \InvalidArgumentException('Value "phone" can not be empty.');
        }
        if (!preg_match("/^\+38\(\d{3}\)\d{3}-\d{2}-\d{2}$/", $phone)) {
            throw new \InvalidArgumentException('Value "phone" has wrong format.');
        }

        $this->phone = $phone;
    }

    public function getPhone() { return $this->phone; }
}