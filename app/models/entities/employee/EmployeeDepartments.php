<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.11.17
 * Time: 21:23
 */

namespace models\entities\employee;


class EmployeeDepartments
{
    private $departments = [];

    public function __construct(array $departments)
    {
        if (empty($departments)) {
            throw new \InvalidArgumentException('Value "departments" must contain at least one department.');
        }

        foreach ($departments as $department) {
            $this->add($department);
        }
    }


    public function add(EmployeeDepartment $department)
    {
        foreach ($this->departments as $item) {
            if ($item->isEqualTo($department)) {
                throw new \DomainException('The department already exists in the collection.');
            }
        }
        $this->departments[] = $department;
    }

    public function remove($index)
    {
        if (!isset($this->departments[$index])) {
            throw new \DomainException('Department not found in collection');
        }
        if (count($this->departments) === 1) {
            throw new \DomainException('Cannot remove the last one. Value "departments" must contain at least one department.');
        }

        $department = $this->departments[$index];
        unset($this->departments[$index]);
        return $department;
    }

    public function getAll()
    {
        return $this->departments;
    }
}