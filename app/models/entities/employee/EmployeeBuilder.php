<?php

namespace models\entities\employee;


class EmployeeBuilder
{
    public function build(array $user)
    {
        try {
            $departmentsCollection = [];
            if (isset($user['departmentIds'])) {
                if (!is_array($user['departmentIds'])) {
                    $user['departmentIds'] = explode(',', $user['departmentIds']);
                }
                foreach ($user['departmentIds'] as $department) {
                    $departmentsCollection[] = new EmployeeDepartment($department);
                }
            }

            $employee = new Employee(
                new EmployeeId($user['id']),
                new EmployeeLogin($user['login']),
                new EmployeePassword($user['password']),
                new EmployeeName($user['last'], $user['first'], $user['middle']),
                new EmployeeDate($user['date_birth']),
                new EmployeeEmail($user['email']),
                new EmployeePhone($user['phone']),
                new EmployeeDate($user['date_employment']),
                new EmployeeDateForFired($user['date_fired']),
                $departmentsCollection,
                new EmployeeStatus($user['status']),
                new EmployeeRole($user['roles'])
            );
        } catch (\Exception $e) {
            // TODO here could be implemented Logger of exceptions, but for this particular task, exceptions would be shown on front page
            $_SESSION["errorFlashMessage"] = $e->getMessage() . ' For employee with id: ' . $user['id'];
            $_SESSION["errorSingleFlashMessage"] = $e->getMessage();
            return;
        }


        return $employee;
    }
}