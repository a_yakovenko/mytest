<?php
/**
 * Created by PhpStorm.
 * User: alex
 * email: 22.11.17
 * Time: 21:03
 */

namespace models\entities\employee;


class EmployeeEmail
{
    private $email;

    public function __construct($email = null)
    {
        if (empty($email)) {
            throw new \InvalidArgumentException('Value "email" can not be empty.');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Value "email" has wrong format.');
        }

        $this->email = $email;
    }

    public function getEmail() { return $this->email; }
}