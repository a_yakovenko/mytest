<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 12:28
 */

namespace models\entities\employee;


class EmployeeLogin
{
    private $login;

    public function __construct($login = null)
    {
        if (empty($login)) {
            throw new \InvalidArgumentException('Value "login" can not be empty.');
        }
        if (strlen($login) < 3) {
            throw new \InvalidArgumentException('Value "login" too short.');
        }
        if (!preg_match('/^[a-zA-Z0-9_-]+$/', $login)) {
            throw new \InvalidArgumentException('Value "login" cannot contain illegal values.');
        }

        $this->login = $login;
    }

    public function getLogin() { return $this->login; }
}