<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 13:30
 */

namespace models\entities\employee;


class EmployeePassword
{
    private $password;

    public function __construct($password = null)
    {
        if (empty($password)) {
            throw new \InvalidArgumentException('Value "password" can not be empty.');
        }
        if (strlen($password) < 6) {
            throw new \InvalidArgumentException('Value "password" too short.');
        }

        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function verifyPassword($password = null): bool
    {
        return password_verify($password, $this->password);
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}