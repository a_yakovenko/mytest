<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 19:38
 */

namespace models\entities\employee;

use models\entities\Aggregate;

class Employee implements Aggregate
{
    private $id;
    private $login;
    private $password;
    private $name;
    private $date_birth;
    private $email;
    private $phone;
    private $date_employment;
    private $date_fired;
    private $departments = [];
    private $status;
    private $role;

    public function __construct(EmployeeId $id, EmployeeLogin $login, EmployeePassword $password, EmployeeName $name, EmployeeDate $date_birth, EmployeeEmail $email, EmployeePhone $phone, EmployeeDate $date_employment, EmployeeDateForFired $date_fired, array $departmentArray, EmployeeStatus $status, EmployeeRole $role)
    {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
        $this->name = $name;
        $this->date_birth = $date_birth;
        $this->email = $email;
        $this->phone = $phone;
        $this->date_employment = $date_employment;
        $this->date_fired = $date_fired;
        $this->departments = new EmployeeDepartments($departmentArray);
        $this->status = $status;
        $this->role = $role;
    }

    public function getId() { return $this->id; }
    public function getLogin() { return $this->login; }
    public function getPassword() { return $this->password; }
    public function getName() { return $this->name; }
    public function getDateBirth() { return $this->date_birth; }
    public function getEmail() { return $this->email; }
    public function getPhone() { return $this->phone; }
    public function getDateEmployment() { return $this->date_employment; }
    public function getDateFired() { return $this->date_fired; }
    public function getDepartments() { return $this->departments->getAll(); }
    public function getStatus() { return $this->status; }
    public function getRole() { return $this->role; }

    public function isActive()
    {
        return $this->status->isActive();
    }

    public function isAdmin()
    {
        return $this->role->isAdmin();
    }
}