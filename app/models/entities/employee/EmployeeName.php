<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 20:32
 */

namespace models\entities\employee;


class EmployeeName
{
    private $last;
    private $first;
    private $middle;

    public function __construct($last = null, $first = null, $middle = null)
    {
        if (empty($last) || empty($first)) {
            throw new \InvalidArgumentException('Values "last" and "first" can not be empty.');
        }
        $this->last = $last;
        $this->first = $first;
        $this->middle = $middle;
    }
    public function getFull()
    {
        return trim($this->last . ' ' . $this->first . ' ' . $this->middle);
    }
    public function getFirst() { return $this->first; }
    public function getMiddle() { return $this->middle; }
    public function getLast() { return $this->last; }
}