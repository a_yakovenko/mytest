<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.11.17
 * Time: 21:20
 */

namespace models\entities\employee;


class EmployeeDepartment
{
    private $departmentID;

    public function __construct(int $departmentID)
    {
        $this->departmentID = $departmentID;
    }

    public function isEqualTo(self $department)
    {
        return $this->departmentID === $department->departmentID;
    }

    public function getDepartmentID() { return $this->departmentID; }
}