<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.11.17
 * Time: 19:42
 */

namespace models\entities;


interface Aggregate
{
    public function getId();
    public function isActive();
    public function isAdmin();
}