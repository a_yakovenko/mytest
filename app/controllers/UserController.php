<?php


namespace controllers;

use models\Helper;
use models\repository\EmployeeRepository;
use models\User;

class UserController
{
    public function actionLoginHandler()
    {
        if (User::isLoggedIn()) {
            Helper::goToMainPage();
        }


        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        try {
            $employeeRepository = new EmployeeRepository();
            $userID = $employeeRepository->getIdByLogin($_POST['login']);
            if (!$userID) {
                throw new \DomainException('User not found');
            }
            $user = new User($userID);
            if (!$user->employee->getPassword()->verifyPassword($_POST['password'])) {
                throw new \DomainException('You entered wrong password');
            }
            $user->rememberUser();
            Helper::goToMainPage();


        } catch (\Exception $e) {
            $_SESSION["errorFlashMessage"] = $e->getMessage();
        }

        }

        require_once(ROOT . '/app/views/front/login.php');
        Helper::deleteFlashes();

        return true;
    }

    public function actionLogoutHandler()
    {
        User::forgotUser();
        Helper::goToMainPage();
    }
}