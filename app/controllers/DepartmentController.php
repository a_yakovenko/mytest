<?php
namespace controllers;

use models\User;
use models\Helper;
use models\repository\DepartmentsRepository;

class DepartmentController
{
    public function actionIndex()
    {
        $departmentsRepository = new DepartmentsRepository();
        $departments = $departmentsRepository->getAll();


        require_once(ROOT . '/app/views/front/department/main.php');
        Helper::deleteFlashes();

        return true;
    }

    public function actionEditDepartment($id)
    {
        $departmentsRepository = new DepartmentsRepository();
        $department = $departmentsRepository->get($id);

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if ($departmentsRepository->update($department['id'], $_POST['department'])) {
                $_SESSION["flashMessage"] = "Department updeted successfully";
                $host = 'http://' . $_SERVER['HTTP_HOST'] . '/departments';
                header('Location:' . $host);
                exit();
            }
        }

        require_once(ROOT . '/app/views/front/department/edit.php');
        Helper::deleteFlashes();

        return true;
    }

    public function actionDeleteDepartment($id)
    {
        $departmentsRepository = new DepartmentsRepository();
        if ($departmentsRepository->remove($id)) {
            $_SESSION["flashMessage"] = "Department deleted successfully";
        }
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/departments';
        header('Location:' . $host);
        exit();
    }

    public function actionAddDepartment()
    {
        $departmentsRepository = new DepartmentsRepository();
        if ($departmentsRepository->add($_POST['department'])) {
            $_SESSION["flashMessage"] = "Department added successfully";
        }
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/departments';
        header('Location:' . $host);
        exit();
    }
}