<?php
namespace controllers;

class ErrorController
{
    public function actionErrorHandler()
    {
        header("HTTP/1.x 404 Not Found");
        require_once(ROOT . '/app/views/front/error.php');

        return true;
    }
}
?>