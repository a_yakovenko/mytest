<?php
namespace controllers;

use models\User;
use models\Helper;
use models\entities\employee\{
    Employee, EmployeeBuilder, EmployeeId, EmployeeRole, EmployeeStatus
};
use models\repository\{
    DepartmentsRepository, EmployeeRepository
};

class MainController
{
    public function actionIndex()
	{
        $employeeRepository = new EmployeeRepository();
        $employeeBuilder = new EmployeeBuilder();
        $usersCollection = [];
        foreach ($employeeRepository->getAll() as $user) {
            $employee = $employeeBuilder->build($user);
            if ($employee instanceof Employee)
                $usersCollection[] = $employee;
        }

        $roles = EmployeeRole::getAllRoles();
        $statuses = EmployeeStatus::getAllStatuses();

        $departmentsRepository = new DepartmentsRepository();
        $departments = $departmentsRepository->getAll();


        require_once(ROOT . '/app/views/front/employee/main.php');
        Helper::deleteFlashes();

		return true;
	}

    public function actionAdduser()
    {
        if (!User::isAdmin()) {
            Helper::goToMainPage();
        }

        $employeeRepository = new EmployeeRepository();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $employeeBuilder = new EmployeeBuilder();
            $employee = $employeeBuilder->build($_POST);

            if ($employee && $employeeRepository->add($employee)) {
                $_SESSION["flashMessage"] = "Employee added successfully";
                Helper::goToMainPage();
            }
        }

        $roles = EmployeeRole::getAllRoles();
        $statuses = EmployeeStatus::getAllStatuses();

        $departmentsRepository = new DepartmentsRepository();
        $departments = $departmentsRepository->getAll();

        require_once(ROOT . '/app/views/front/employee/add.php');
        Helper::deleteFlashes();

        return true;
    }

    public function actionEdituser($id)
    {
        if (!User::isAdmin()) {
            Helper::goToMainPage();
        }

        $employeeRepository = new EmployeeRepository();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $employeeBuilder = new EmployeeBuilder();
            $employee = $employeeBuilder->build($_POST);

            if ($employee && $employeeRepository->save($employee)) {
                $_SESSION["flashMessage"] = "Employee edited successfully";
                Helper::goToMainPage();
            }
        }

        $employee = $employeeRepository->get(new EmployeeId($id));
        $employeeDepartmentIds = explode(',', $employee['departmentIds']);

        $roles = EmployeeRole::getAllRoles();
        $statuses = EmployeeStatus::getAllStatuses();

        $departmentsRepository = new DepartmentsRepository();
        $departments = $departmentsRepository->getAll();

        require_once(ROOT . '/app/views/front/employee/edit.php');
        Helper::deleteFlashes();

        return true;
    }


    public function actionViewuser($id)
    {
        $employeeRepository = new EmployeeRepository();
        $employeeBuilder = new EmployeeBuilder();

        $employee = $employeeRepository->get(new EmployeeId($id));
        $user = $employeeBuilder->build($employee);

        $roles = EmployeeRole::getAllRoles();
        $statuses = EmployeeStatus::getAllStatuses();

        $departmentsRepository = new DepartmentsRepository();
        $departments = $departmentsRepository->getAll();

        require_once(ROOT . '/app/views/front/employee/view.php');
        Helper::deleteFlashes();

        return true;
    }

    public function actionDeleteuser($id)
    {
        if (!User::isAdmin()) {
            Helper::goToMainPage();
        }

        $employeeRepository = new EmployeeRepository();
        $employeeBuilder = new EmployeeBuilder();

        $employee = $employeeRepository->get(new EmployeeId($id));
        $user = $employeeBuilder->build($employee);

        if ($user instanceof Employee) {
            if ($employeeRepository->remove($user)) {
                $_SESSION["flashMessage"] = "Employee deleted successfully";
                Helper::goToMainPage();
            }
        }
    }
}
?>