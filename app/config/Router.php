<?php
namespace config;

// класс маршрутизатора
class Router
{
	private $routes;

	public function __construct()
	{
		$routesPath = ROOT . '/app/config/routes.php';
		$this->routes = require_once($routesPath);
	}

	private function getUrl()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
			return trim($_SERVER['REQUEST_URI']);
		}
	}

	private function ErrorPage404()
	{
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
	}

	public function run()		// метод подключает файлы классов контроллеров
	{
        

		$result = null;
		$url = $this->getUrl();

        if (\models\User::isLoggedIn() == false && $url != '/login') {
            $host = 'http://' . $_SERVER['HTTP_HOST'] . '/login';
            header('Location:' . $host);
            exit();
        }

		foreach ($this->routes as $urlPattern => $path) {
			if (preg_match("~$urlPattern~", $url)) {
				$internalRoute = preg_replace("~$urlPattern~", $path, $url);

				$segments = explode('/', $internalRoute);	
				$controllerName = "controllers" . '\\' . ucfirst(array_shift($segments)) . 'Controller';
				$actionName = 'action' . ucfirst(array_shift($segments));

				$controllerObject = new $controllerName;
				
				// Вызываем метод $controllerObject->$actionName с массивом параметров
				$result = call_user_func_array(array($controllerObject, $actionName), $segments);

				if ($result === true) {
					break;
				}
			}
		}
		if ($result == null) {
			$this->ErrorPage404();
		}
	}
}
?>