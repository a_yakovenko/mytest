<?php
// path pattern => controller/action
return array(

	'^/404\z' 						=> 'error/errorHandler',

    '^/login\z'						=> 'user/loginHandler',

    '^/logout\z'					=> 'user/logoutHandler',

    '^/editdepartment/([0-9]+\z)'   => 'department/editDepartment/$1',

    '^/deletedepartment/([0-9]+\z)' => 'department/deleteDepartment/$1',

    '^/adddepartment\z'				=> 'department/addDepartment',

    '^/departments\z'				=> 'department/index',




    '^/deleteuser/([0-9]+\z)'       => 'main/deleteuser/$1',

    '^/viewuser/([0-9]+\z)'         => 'main/viewuser/$1',

    '^/edituser/([0-9]+\z)'         => 'main/edituser/$1',

    '^/adduser\z'					=> 'main/adduser',

	'^/\z'							=> 'main/index'
);
?>