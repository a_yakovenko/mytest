<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>

<?php require_once(ROOT . '/app/views/front/common/sidebar.php'); ?>


<div class="right_side">

    <div class="section_2">
        <table class="user_info" cellpadding="0" cellspacing="0">
            <tr class="row_width">
                <td>
                    <span>
                        <b>Login</b>
                    </span>
                </td>
                <td>
                  <span>
                     <?= $user->getLogin()->getLogin(); ?>
                  </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                   <span>
                        <b>ФИО</b>
                   </span>
                </td>
                <td>
                  <span>
                        <?= $user->getName()->getFull(); ?>
                  </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                     <b>Дата Рождения</b>
                    </span>
                </td>
                <td>
                  <span>
                     <?= $user->getDateBirth()->getDate(); ?>
                  </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>Телефон</b>
                    </span>
                </td>
                <td>
                   <span>
                      <?= $user->getPhone()->getPhone(); ?>
                   </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>E-mail</b>
                    </span>
                </td>
                <td>
                   <span>
                        <?= $user->getEmail()->getEmail(); ?>
                   </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>Дата Трудоустройства</b>
                    </span>
                </td>
                <td>
                   <span>
                       <?= $user->getDateEmployment()->getDate(); ?>
                   </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>Дата Увольнения</b>
                    </span>
                </td>
                <td>
                   <span>
                       <?= $user->getDateFired()->getDate(); ?>
                   </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>Статус</b>
                    </span>
                </td>
                <td>
                   <span>
                       <?= ($user->getStatus()->isActive()) ? 'Активный сотрудник' : 'Не активный сотрудник'; ?>
                   </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>Отдел</b>
                    </span>
                </td>
                <td>
                   <span>
                      <?php foreach ($userDepartments = $user->getDepartments() as $item): ?>
                          <?= $departments[$item->getDepartmentID()]; ?>
                          <?php if (end($userDepartments) != $item) echo ', '; ?>
                      <?php endforeach; ?>
                   </span>
                </td>
            </tr>
            <tr class="row_width">
                <td>
                    <span>
                        <b>Роль</b>
                    </span>
                </td>
                <td>
                   <span>
                      <?= $roles[$user->getRole()->getRole()]; ?>
                   </span>
                </td>
            </tr>
        </table>
    </div>
    <?php if (\models\User::isAdmin()): ?>
    <a href="/edituser/<?= $user->getId()->getId(); ?>"><button type="submit">
        Редактировать
    </button></a>
    <?php endif; ?>
    <a href="/"><button type="submit">
            На главную
    </button></a>

</div>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>