<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>

<?php require_once(ROOT . '/app/views/front/common/sidebar.php'); ?>


    <div class="right_side">
        <div class="section_3 form_section_3">
            <div class="left_form_side">
                <h5 style="color:red;">
                    <?php echo isset($_SESSION["errorSingleFlashMessage"]) ? $_SESSION["errorSingleFlashMessage"] : ''; ?>
                </h5>
                <form method="POST" action="/edituser/<?= $employee['id']; ?>" class="info_fields">

                        <input name="login" type="hidden" value="<?= (isset($employee['login'])) ? $employee['login'] : ''; ?>">

                    <label >
                        <span>Фамилия</span>
                        <input name="last" type="text" value="<?= (isset($employee['last'])) ? $employee['last'] : ''; ?>" required>
                    </label>
                    <label >
                        <span>Имя</span>
                        <input name="first" type="text" value="<?= (isset($employee['first'])) ? $employee['first'] : ''; ?>" required>
                    </label>
                    <label >
                        <span>Отчество</span>
                        <input name="middle" type="text"value="<?= (isset($employee['middle'])) ? $employee['middle'] : ''; ?>">
                    </label>
                    <label>
                        <span>Дата Рождения</span>
                        <input name="date_birth" type="text" class="datepicker" value="<?= (isset($employee['date_birth'])) ? $employee['date_birth'] : ''; ?>" required>
                    </label>
                    <label>
                        <span>E-mail</span>
                        <input name="email" type="email" value="<?= (isset($employee['email'])) ? $employee['email'] : ''; ?>" required>
                    </label>
                    <label>
                        <span>Телефон</span>
                        <input name="phone" type="tel" class="phone" value="<?= (isset($employee['phone'])) ? substr($employee['phone'], 4) : ''; ?>" required>
                    </label>
                    <label>
                        <span>Дата Трудоустройства</span>
                        <input name="date_employment" name="middle" type="text" class="datepicker" value="<?= (isset($employee['date_employment'])) ? $employee['date_employment'] : ''; ?>" required>
                    </label>
                    <label>
                        <span>Дата Увольнения</span>
                        <input name="date_fired" type="text" class="datepicker" value="<?= (isset($employee['date_fired'])) ? $employee['date_fired'] : ''; ?>" >
                    </label>
                    <input name="id" type="hidden" value="<?php echo $id; ?>">
                    <input name="password" type="hidden" value="dummyy">
                    <span style="display: block;padding:10px 0;">Статус</span>
                    <select name="status" id="choice_1">
                        <?php foreach ($statuses as $key => $value): ?>
                            <?php if ($employee['status'] == $key):?>
                                <option value="<?= $key ?>" selected><?= $value ?></option>
                            <?php else: ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                    <span style="display: block;padding:10px 0;">Отдел</span>
                    <select name="departmentIds[]" id="choice_2" multiple="multiple">
                        <?php foreach ($departments as $key => $value): ?>
                            <?php foreach ($employeeDepartmentIds as $departmentId): ?>
                                <?php if ($departmentId == $key):?>
                                    <option value="<?= $key ?>" selected><?= $value ?></option>
                                    <?php continue 2; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span style="display: block;padding:10px 0;">Роль</span>
                    <select name="roles" id="choice_1">
                        <?php foreach ($roles as $key => $value): ?>
                            <?php if ($employee['roles'] == $key):?>
                                <option value="<?= $key ?>" selected><?= $value ?></option>
                            <?php else: ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                        <button type="submit">
                            Редактировать
                        </button>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $( function() {
                $(".datepicker").datepicker({
                    dateFormat: 'yy-mm-dd'
                });
            } );
            $('.phone').mask('+38(000)000-00-00', {placeholder: "+38(__)___-__-__"});
        });
    </script>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>