<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>

<?php require_once(ROOT . '/app/views/front/common/sidebar.php'); ?>


<div class="right_side">
    <div class="section_3 form_section_3">
        <div class="left_form_side">
            <h5 style="color:red;">
                <?php echo isset($_SESSION["errorSingleFlashMessage"]) ? $_SESSION["errorSingleFlashMessage"] : ''; ?>
            </h5>
            <form method="POST" action="/adduser" class="info_fields">
                <label >
                    <span>Login</span>
                    <input name="login" type="text" value="<?= (isset($_POST['login'])) ? $_POST['login'] : ''; ?>" required>
                </label>
                <label >
                    <span>Password</span>
                    <input name="password" type="password" required>
                </label>
                <label >
                    <span>Фамилия</span>
                    <input name="last" type="text" value="<?= (isset($_POST['last'])) ? $_POST['last'] : ''; ?>" required>
                </label>
                <label >
                    <span>Имя</span>
                    <input name="first" type="text" value="<?= (isset($_POST['first'])) ? $_POST['first'] : ''; ?>" required>
                </label>
                <label >
                    <span>Отчество</span>
                    <input name="middle" type="text"value="<?= (isset($_POST['middle'])) ? $_POST['middle'] : ''; ?>">
                </label>
                <label>
                    <span>Дата Рождения</span>
                    <input name="date_birth" type="text" class="datepicker" value="<?= (isset($_POST['date_birth'])) ? $_POST['date_birth'] : ''; ?>" required>
                </label>
                <label>
                    <span>E-mail</span>
                    <input name="email" type="email" value="<?= (isset($_POST['email'])) ? $_POST['email'] : ''; ?>" required>
                </label>
                <label>
                    <span>Телефон</span>
                    <input name="phone" type="tel" class="phone" value="<?= (isset($_POST['phone'])) ? substr($_POST['phone'], 4) : ''; ?>" required>
                </label>
                <label>
                    <span>Дата Трудоустройства</span>
                    <input name="date_employment" name="middle" type="text" class="datepicker" value="<?= (isset($_POST['date_employment'])) ? $_POST['date_employment'] : ''; ?>" required>
                </label>
                <label>
                    <span>Дата Увольнения</span>
                    <input name="date_fired" type="text" class="datepicker" value="<?= (isset($_POST['date_fired'])) ? $_POST['date_fired'] : ''; ?>" >
                </label>
                <input name="id" type="hidden" value="1111">
                <span style="display: block;padding:10px 0;">Статус</span>
                <select name="status" id="choice_1">
                    <?php foreach ($statuses as $key => $value): ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>
                <span style="display: block;padding:10px 0;">Отдел</span>
                <select name="departmentIds[]" id="choice_2" multiple="multiple">
                    <?php foreach ($departments as $key => $value): ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>
                <span style="display: block;padding:10px 0;">Роль</span>
                <select name="roles" id="choice_1">
                    <?php foreach ($roles as $key => $value): ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>
                <button type="submit">
                    Создать
                </button>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $( function() {
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        } );
        $('.phone').mask('+38(000)000-00-00', {placeholder: "+38(__)___-__-__"});
    });
</script>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>