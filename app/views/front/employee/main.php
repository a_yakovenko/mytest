<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>

<?php require_once(ROOT . '/app/views/front/common/sidebar.php'); ?>


<div class="right_side">
    <div class="section_1">
        <h5 style="color:green;">
            <?php echo isset($_SESSION["flashMessage"]) ? $_SESSION["flashMessage"] : ''; ?>
        </h5>
        <h5 style="color:red;">
            <?php echo isset($_SESSION["errorFlashMessage"]) ? $_SESSION["errorFlashMessage"] : ''; ?>
        </h5>
            <table class="user_info" cellpadding="0" cellspacing="0">
                <form action="/" method="POST">
                <tr>
                        <td>
                        </td>
                        <td>
                            <div class="search">
                                <input type="search" name="name" placeholder="Поиск по ФИО" value="<?= (isset($_POST['name'])) ? $_POST['name'] : ''; ?>">
                            </div>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div class="search">
                                <input type="search" name="email" placeholder="Поиск по e-mail" value="<?= (isset($_POST['email'])) ? $_POST['email'] : ''; ?>">
                            </div>
                        </td>
                        <td>
                            <div class="search">
                                <input type="search" name="phone" placeholder="Поиск по телефону" value="<?= (isset($_POST['phone'])) ? $_POST['phone'] : ''; ?>">
                            </div>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div class="search">
                                <select name="status" id="choice_1">
                                    <option value=""></option>
                                    <?php foreach ($statuses as $key => $value): ?>
                                        <?php if (isset($_POST['status']) && $_POST['status'] == $key):?>
                                            <option value="<?= $key ?>" selected><?= $value ?></option>
                                        <?php else: ?>
                                            <option value="<?= $key ?>"><?= $value ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="search">
                                <select name="department" id="choice_1">
                                    <option value=""></option>
                                    <?php foreach ($departments as $key => $value): ?>
                                        <?php if (isset($_POST['department']) && $_POST['department'] == $key):?>
                                            <option value="<?= $key ?>" selected><?= $value ?></option>
                                        <?php else: ?>
                                            <option value="<?= $key ?>"><?= $value ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </td>
                    </td>
                    <td>

                    </td>
                    <td>
                        <button type="submit" class="search_btn">Найти</button>
                    </td>

                </tr>
                </form>

                <tr>
                    <td>
                        <span>
                            <b>Login</b>
                        </span>
                    </td>
                    <td>
                       <span>
                            <b>ФИО</b>
                       </span>
                    </td>
                    <td>
                        <span>
                         <b>Дата Рождения</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>E-mail</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>Телефон</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>Дата Трудоустройства</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>Дата Увольнения</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>Статус</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>Отдел</b>
                        </span>
                    </td>
                    <td>
                        <span>
                            <b>Роль</b>
                        </span>
                    </td>
                    <td>

                    </td>
                </tr>

                <?php foreach ($usersCollection as $user): ?>
                <tr>
                    <td>
                        <span><?= $user->getLogin()->getLogin(); ?></span>
                    </td>
                    <td>
                        <span><?= $user->getName()->getFull(); ?></span>
                    </td>
                    <td>
                        <span><?= $user->getDateBirth()->getDate(); ?></span>
                    </td>
                    <td>
                        <span><?= $user->getEmail()->getEmail(); ?></span>
                    </td>
                    <td>
                        <span><?= $user->getPhone()->getPhone(); ?></span>
                    </td>
                    <td>
                        <span><?= $user->getDateEmployment()->getDate(); ?></span>
                    </td>
                    <td>
                        <span><?= $user->getDateFired()->getDate(); ?></span>
                    </td>
                    <td>
                        <span><?= ($user->getStatus()->isActive()) ? 'Активный сотрудник' : 'Не активный сотрудник'; ?></span>
                    </td>
                    <td>
                        <span>
                            <?php foreach ($userDepartments = $user->getDepartments() as $item): ?>
                                <?= $departments[$item->getDepartmentID()]; ?>
                                <?php if (end($userDepartments) != $item) echo ', '; ?>
                            <?php endforeach; ?>
                        </span>
                    </td>
                    <td>
                        <span><?= $roles[$user->getRole()->getRole()]; ?></span>
                    </td>
                    <td>
                        <span class="watch">
                           <a href="/viewuser/<?= $user->getId()->getId(); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                       </span>
                        <?php if (\models\User::isAdmin()): ?>
                        <span class="edit">
                           <a href="/edituser/<?= $user->getId()->getId(); ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                       </span>

                        <span class="delete">
                           <a href="/deleteuser/<?= $user->getId()->getId(); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                       </span>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>

            </table>
    </div>

</div>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>