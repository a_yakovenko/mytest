<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="utf-8">
    <meta name="description" content="">
    <link rel="stylesheet" href="/web/css/main.css">
    <link rel="stylesheet" href="/web/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>
    <script src="/web/script/jquery-ui.min.js"></script>
    <script src="/web/script/main_js.js"></script>
    <title>BrightTech</title>
</head>

<body>
