<div class="left_side">
    <ol>
        <a href="/logout"><li>Выйти (<?= \models\User::getLoginName() ?>)</li></a>
        <a href="/"><li>Список сотрудников</li></a>
        <?php if(\models\User::isAdmin()): ?>
        <a href="/adduser"><li>Добавить нового сотрудника</li></a>
        <a href="/departments"><li>Редактирование отделов</li></a>
        <?php endif; ?>
    </ol>
</div>