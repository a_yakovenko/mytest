<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>



    <div class="right_side">
        <div class="section_3 form_section_3">
            <div class="left_form_side">
                <h5 style="color:red;">
                    <?php echo isset($_SESSION["errorFlashMessage"]) ? $_SESSION["errorFlashMessage"] : ''; ?>
                </h5>
                <form method="POST" action="/login" class="info_fields">

                    <label >
                        <span>Login</span>
                        <input name="login" type="text" value="" required>
                    </label>

                    <label >
                        <span>Password</span>
                        <input name="password" type="password" value="" required>
                    </label>

                        <button type="submit">
                            Войти
                        </button>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $( function() {
                $(".datepicker").datepicker({
                    dateFormat: 'yy-mm-dd'
                });
            } );
            $('.phone').mask('+38(000)000-00-00', {placeholder: "+38(__)___-__-__"});
        });
    </script>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>