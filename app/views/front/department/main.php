<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>

<?php require_once(ROOT . '/app/views/front/common/sidebar.php'); ?>


<div class="right_side">
    <h5 style="color:green;">
        <?php echo isset($_SESSION["flashMessage"]) ? $_SESSION["flashMessage"] : ''; ?>
    </h5>
    <h5 style="color:red;">
        <?php echo isset($_SESSION["errorFlashMessage"]) ? $_SESSION["errorFlashMessage"] : ''; ?>
    </h5>
    <div class="section_2">
        <table class="user_info" cellpadding="0" cellspacing="0">
        <?php foreach ($departments as $key => $value): ?>
            <tr class="row_width">
                <td>
                    <span>
                            <a href="/editdepartment/<?= $key; ?>">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a href="/deletedepartment/<?= $key; ?>">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td>
                    <span>
                        <?= $value ?>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
        <br>
        <br>
        <form method="POST" action="/adddepartment" class="info_fields">
            <label >
                <span>Добавить Отдел</span>
                <input name="department" type="text" value="" required>
            </label>
            <button type="submit">
                Добавить
            </button>
        </form>
    </div>

</div>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>