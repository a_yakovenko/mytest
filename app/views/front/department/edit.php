<?php require_once(ROOT . '/app/views/front/common/header.php'); ?>

<?php require_once(ROOT . '/app/views/front/common/sidebar.php'); ?>


<div class="right_side">
    <h5 style="color:red;">
        <?php echo isset($_SESSION["errorFlashMessage"]) ? $_SESSION["errorFlashMessage"] : ''; ?>
    </h5>
    <div class="section_2">
        <form method="POST" action="/editdepartment/<?= $department['id'] ?>" class="info_fields">
            <label >
                <span>Редактировать отдел</span>
                <input name="department" type="text" value="<?= $department['title'] ?>" required>
            </label>
            <button type="submit">
                Отправить
            </button>
        </form>
    </div>

</div>

<?php require_once(ROOT . '/app/views/front/common/footer.php'); ?>