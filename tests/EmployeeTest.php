<?php
use models\entities\employee\{
    EmployeeDepartment, EmployeeId, EmployeeRole
};
use tests\entities\employee\EmployeeBuilder;

require_once __DIR__ . '/../app/config/config.php';


class EmployeeTest extends \PHPUnit_Framework_TestCase
{
    public function test_Create_Employee()
    {
        $employeeBuilder = EmployeeBuilder::instance();
        $employee = $employeeBuilder->build();

        $this->assertEquals($employeeBuilder->id, $employee->getId());
        $this->assertEquals($employeeBuilder->login, $employee->getLogin());
        $this->assertEquals($employeeBuilder->name, $employee->getName());
        $this->assertEquals($employeeBuilder->date_birth, $employee->getDateBirth());
        $this->assertEquals($employeeBuilder->email, $employee->getEmail());
        $this->assertEquals($employeeBuilder->phone, $employee->getPhone());
        $this->assertEquals($employeeBuilder->date_employment, $employee->getDateEmployment());
        $this->assertEquals($employeeBuilder->date_fired, $employee->getDateFired());
        $this->assertEquals($employeeBuilder->departments, $employee->getDepartments());
        $this->assertEquals($employeeBuilder->status, $employee->getStatus());
        $this->assertEquals($employeeBuilder->role, $employee->getRole());
        $this->assertEquals(EmployeeRole::ADMIN, $employee->getRole()->getRole());

        $this->assertTrue($employee->getPassword()->verifyPassword(123456));
        $this->assertTrue($employee->isActive());
        $this->assertTrue($employee->isAdmin());

        $this->assertNotNull($employee->getDateBirth()->getDate());
        $this->assertNotNull($employee->getDateEmployment()->getDate());

        $this->assertNull($employee->getDateFired()->getDate());

        $this->assertCount(count($employeeBuilder->departments), $employee->getDepartments());
    }


    public function test_Without_Departments()
    {
        $this->expectExceptionMessage('Value "departments" must contain at least one department.');

        $employeeBuilder = EmployeeBuilder::instance();
        $employeeBuilder->departments = [];
        $employeeBuilder->build();
    }


    public function test_With_The_Same_Departments()
    {
        $this->expectExceptionMessage('The department already exists in the collection.');

        $employeeBuilder = EmployeeBuilder::instance();
        $employeeBuilder->departments = [
            new EmployeeDepartment(1),
            new EmployeeDepartment(3),
            new EmployeeDepartment(3)
        ];
        $employeeBuilder->build();
    }


    public function test_Is_Equal()
    {
        $firstID = new EmployeeId(555);
        $secondID = new EmployeeId(555);

        $this->assertTrue($firstID->isEqualTo($secondID));
    }
}