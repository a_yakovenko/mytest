<?php
namespace tests\entities\employee;

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.11.17
 * Time: 15:52
 */

use models\entities\employee\{
    Employee, EmployeeDate, EmployeeDateForFired, EmployeeDepartment, EmployeeEmail, EmployeeId, EmployeeLogin, EmployeeName, EmployeePassword, EmployeePhone, EmployeeRole, EmployeeStatus
};

class EmployeeBuilder
{
    public $id;
    public $login;
    public $password;
    public $name;
    public $date_birth;
    public $email;
    public $phone;
    public $date_employment;
    public $date_fired;
    public $departments = [];
    public $status;
    public $role;

    public function __construct()
    {
        $this->id = new EmployeeId(1);
        $this->login = new EmployeeLogin('Alex');
        $this->password = new EmployeePassword('123456');
        $this->name = new EmployeeName('Яковенко', 'Александр');
        $this->date_birth = new EmployeeDate('1989-02-28');
        $this->email = new EmployeeEmail('aleksandr.yakovenko.ukr@gmail.com');
        $this->phone = new EmployeePhone('+38(000)000-00-00');
        $this->date_employment = new EmployeeDate('2018-01-01');
        $this->date_fired = new EmployeeDateForFired();
        $this->departments = [
            new EmployeeDepartment(1),
            new EmployeeDepartment(3),
        ];
        $this->status = new EmployeeStatus();
        $this->role = new EmployeeRole(EmployeeRole::ADMIN);
    }

    public static function instance()
    {
        return new self();
    }

    public function build()
    {
        $employee = new Employee(
            $id = $this->id,
            $login = $this->login,
            $password = $this->password,
            $name = $this->name,
            $date_birth = $this->date_birth,
            $email = $this->email,
            $phone = $this->phone,
            $date_employment = $this->date_employment,
            $date_fired = $this->date_fired,
            $departments = $this->departments,
            $status = $this->status,
            $role = $this->role
        );
        return $employee;
    }
}