-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 10:14 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brightech`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `title`) VALUES
(1, 'Отдел Маркетинга'),
(2, 'Отдел Логистики'),
(3, 'Отдел Продаж'),
(4, 'Отдел Разработки'),
(14, 'Отдел Производства');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last` varchar(255) NOT NULL,
  `first` varchar(255) NOT NULL,
  `middle` varchar(255) DEFAULT NULL,
  `date_birth` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(17) NOT NULL,
  `date_employment` date NOT NULL,
  `date_fired` date DEFAULT NULL,
  `status` int(1) NOT NULL,
  `roles` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `last`, `first`, `middle`, `date_birth`, `email`, `phone`, `date_employment`, `date_fired`, `status`, `roles`) VALUES
(2, 'Alex', '$2y$10$gly6Of03.FdxzIOBPqfxiOqw8/JmqVb5Vb4hbS8eCsvfdNtO7NPTO', 'Яковенко', 'Александр', 'Владимирович', '1989-02-28', 'test@gmail.com', '+38(000)000-00-00', '2017-11-01', NULL, 1, 2),
(3, 'regular_user_1', '$2y$10$9PTpDNS9pv16wt57bTUDbeBJDCLbQVgwn2Gm1kawN9egY.bV4Xu9q', 'Иванов', 'Иван', '', '1970-11-15', 'ivanov@ivanich.net', '+38(000)000-00-00', '2017-10-01', '2017-10-24', 2, 1),
(37, 'Alex2', '$2y$10$RpcWnY5dTXSCbar4k57BdOavKz/VU4zzpKSB1xfJAkapweUhy1KiO', 'Петров', 'Петр', 'Петрович', '2017-11-05', 'petrov@ramble.ru', '+38(093)999-88-77', '2017-11-06', '2017-11-06', 2, 1),
(38, 'User1', '$2y$10$yAPDCxd9Z0m8R3PgTIhoGOpu/eFNAKvz4owScpgd12kbTHfjDOKqO', 'Familia', 'UserName', 'UserOtchstvo', '2017-11-01', 'usertest@gmail.com', '+38(093)000-55-88', '2017-11-03', NULL, 1, 1),
(39, 'user2', '$2y$10$vTIKd4Sp7DoTFuljzLDwueoKysi6vP3I08ZGQsAv1emLbIEAUjcfS', 'Familia', 'Alex', '', '2017-11-01', 'usertest@gmail.com', '+38(000)111-33-22', '2017-11-22', '2017-11-23', 2, 1),
(40, 'user3', '$2y$10$hrIVAN8.oUR5pzp3BjTsHuz2Hj05W7uapkvVmm.7/2QWbUkT2XWa6', 'Yakovenko', 'Aleksandr', 'Vladimirovich', '2017-11-01', 'aleksndr@aleks.ru', '+38(888)888-88-88', '2017-11-01', NULL, 1, 1),
(41, 'user4', '$2y$10$tGjNo9PXcYBuLoW8o6XdtOFZLadWmgKubeIIVQpc4KCLbIsH1asPC', 'Familia', 'Imya', 'Otchestvo', '2017-11-01', 'dssdfsf@sdfsd.ru', '+38(111)111-11-11', '2017-11-08', NULL, 1, 1),
(42, 'User5', '$2y$10$EgzjYQ8wilipOC3x7YDsQe9t2hUyOQD7P0.Oh9bODKFkH2IJhQexq', 'Familia', 'Aleksandr', '', '2017-11-11', 'test@test.ru', '+38(111)111-11-11', '2017-11-03', NULL, 1, 1),
(44, 'User7', '$2y$10$F.m1joMfRZ1eu5xV4oHj/eeAmhos2mkVM9Cb7Odd1/8cPfR6hcgBy', 'Familia', 'Alex', '', '2017-11-01', 'test@test.ru', '+38(999)999-99-99', '2017-11-15', '2017-11-01', 2, 1),
(50, 'User10', '$2y$10$gly6Of03.FdxzIOBPqfxiOqw8/JmqVb5Vb4hbS8eCsvfdNtO7NPTO', 'Familia', 'Alex', 'Otchestvo', '2017-11-02', 'test@test.ru', '+38(111)111-11-11', '2017-11-14', '2017-11-15', 1, 1),
(51, 'User11', '$2y$10$60359wZEkGPPiOcnMOeMSOPAPhQDYtGSGTh5qSXYzZkz6UdPCRHcy', 'Familia', 'bb', 'Otchestvo', '2017-11-13', 'te@sdfs.ru', '+38(111)111-11-11', '2017-11-09', '2017-11-23', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_department`
--

CREATE TABLE `users_department` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_department`
--

INSERT INTO `users_department` (`id`, `user_id`, `department_id`) VALUES
(18, 42, 3),
(20, 44, 2),
(91, 40, 2),
(95, 50, 1),
(96, 50, 4),
(99, 51, 2),
(100, 51, 3),
(101, 2, 1),
(102, 2, 2),
(105, 38, 1),
(106, 38, 2),
(113, 37, 2),
(114, 37, 3),
(115, 3, 2),
(116, 3, 3),
(118, 41, 2),
(119, 41, 4),
(120, 41, 14),
(123, 39, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Indexes for table `users_department`
--
ALTER TABLE `users_department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `department_id` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `users_department`
--
ALTER TABLE `users_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_department`
--
ALTER TABLE `users_department`
  ADD CONSTRAINT `fk-department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `fk-user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
