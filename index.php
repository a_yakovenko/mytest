<?php 
session_start();
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

define('ROOT', $_SERVER['DOCUMENT_ROOT']);

require_once ROOT . '/app/config/config.php';
require_once ROOT . '/vendor/autoload.php';
$router = new config\Router();
$router->run();
?>